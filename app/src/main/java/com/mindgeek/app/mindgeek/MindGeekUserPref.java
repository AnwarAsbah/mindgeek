package com.mindgeek.app.mindgeek;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Created by anwar.asbah on 2019-01-22.
 */

public class MindGeekUserPref {

    private static MindGeekUserPref mindGeekUserPref;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final String PASSCODE_ENABLED_KEY = "passcode_enabled_key";
    private static final String PASSCODE_VALUE_KEY = "passcode_value";
    private static final String APP_LOCKED_KEY = "app_locked_key";
    private static final String APP_LOCK_TIME_KEY = "app_lock_time_key";
    private static final String NOTES_KEY = "notes_key";

    private MindGeekUserPref(Context context) {
        preferences = context.getSharedPreferences("mindGeek", Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public static MindGeekUserPref getInstance(Context context) {
        if (mindGeekUserPref == null) {
            mindGeekUserPref = new MindGeekUserPref(context);
        }
        return mindGeekUserPref;
    }

    public void setPasscodeEnabled(boolean enabled) {
        editor.putBoolean(PASSCODE_ENABLED_KEY, enabled);
        editor.commit();
    }

    public boolean isPasscodeEnabled() {
        return preferences.getBoolean(PASSCODE_ENABLED_KEY, false);
    }

    public void setPasscode(String passcode) {
        editor.putString(PASSCODE_VALUE_KEY, passcode);
        editor.commit();
    }

    public String getPasscode() {
        return preferences.getString(PASSCODE_VALUE_KEY, "");
    }

    public void setAppLocked(boolean locked) {
        editor.putBoolean(APP_LOCKED_KEY, locked);
        editor.putLong(APP_LOCK_TIME_KEY, System.currentTimeMillis());
        editor.commit();
    }

    public boolean isAppLocked() {
        boolean isLocked = preferences.getBoolean(APP_LOCKED_KEY, false);
        if (isLocked) {
            if (System.currentTimeMillis() - preferences.getLong(APP_LOCK_TIME_KEY, 0) > LockService.LOCK_PERIOD) {
                setAppLocked(false);
                return false;
            }

            return true;
        }
        return false;
    }

    public long getAppLockTime() {
        return preferences.getLong(APP_LOCK_TIME_KEY, System.currentTimeMillis());
    }

    public void addNote (Context context, String note) {
        String notes = preferences.getString(NOTES_KEY, "");

        if(notes.indexOf(note) != -1) {
            Toast.makeText(context, "Note already Exists", Toast.LENGTH_LONG).show();
            return;

        }
        notes += (notes.length() == 0 ? "" : ",");
        notes += note;
        editor.putString(NOTES_KEY, notes);
        editor.commit();
    }

    public void removeNote (String note) {
        String notes = preferences.getString(NOTES_KEY, "");
        notes = notes.replace(note, "");
        notes = notes.replace(",,", ",");
        notes = ";" + notes + ";";
        notes = notes.replace(";,", "");
        notes = notes.replace(",;", "");
        notes = notes.replace(";", "");
        editor.putString(NOTES_KEY, notes);
        editor.commit();
    }

    public String[] getNotes () {
        String notes = preferences.getString(NOTES_KEY, "");
        if(notes.length() == 0) {
            return null;
        }
        String[] notesList = notes.split(",");
        return notesList;
    }

}
