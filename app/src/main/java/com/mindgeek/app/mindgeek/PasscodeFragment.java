package com.mindgeek.app.mindgeek;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

public class PasscodeFragment extends Fragment {


    private static final String TYPE_PARAM = "type_param";
    private static final int MAX_AVAILABLE_ATTEMPTS = 3;

    private PassCodeFragmentType typeParam;
    private int attempts = 0;

    private TextView title;
    private EditText passcodeEditText;
    private View alertMsg;
    private View lockMsg;
    private ViewGroup screenView;

    private PasscodeFragmentCallback mListener;

    public class PasscodeTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() == 4) {
                if (mListener != null) {
                    mListener.onPasscodeEntered(charSequence.toString(), typeParam);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }

    public PasscodeFragment() {
        // Required empty public constructor
    }

    public static PasscodeFragment newInstance(PassCodeFragmentType type) {
        Bundle args = new Bundle();
        if (type != null) {
            args.putInt(TYPE_PARAM, type.ordinal());
        }

        PasscodeFragment fragment = new PasscodeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            Bundle bundle = getArguments();
            typeParam = PassCodeFragmentType.ENTER_PASSWORD;
            if (bundle.containsKey(TYPE_PARAM)) {
                typeParam = PassCodeFragmentType.values()[bundle.getInt(TYPE_PARAM)];
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_passcode, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
    }

    private void initView(View view) {
        title = view.findViewById(R.id.title);
        passcodeEditText = view.findViewById(R.id.passcode_edit_text);
        alertMsg = view.findViewById(R.id.error_msg_text_view);
        lockMsg = view.findViewById(R.id.lock_msg_text_view);
        screenView = view.findViewById(R.id.passcode_view);

        title.setText(typeParam.screenTitleID);
        passcodeEditText.addTextChangedListener(new PasscodeTextWatcher());

        showKeyboard();

    }

    private void showKeyboard() {
        passcodeEditText.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(passcodeEditText, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onResume() {
        super.onResume();

        updateLockMode();
    }

    public void updateLockMode() {
        if (MindGeekUserPref.getInstance(getContext()).isAppLocked()) {
            lockApp();
        } else {
            unlockApp();
        }
    }

    private void lockApp() {
        TransitionSet set = new TransitionSet();
        set.addTransition(new Slide(Gravity.RIGHT));
        TransitionManager.beginDelayedTransition(screenView, set);

        attempts = 0;
        lockMsg.setVisibility(View.VISIBLE);
        passcodeEditText.setEnabled(false);
        passcodeEditText.setText("");

        if (isAdded() && getContext() != null) {
            InputMethodManager manger = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            manger.hideSoftInputFromWindow(passcodeEditText.getWindowToken(), 0);
        }
    }

    private void unlockApp() {
        TransitionSet set = new TransitionSet();
        set.addTransition(new Slide(Gravity.RIGHT));
        TransitionManager.beginDelayedTransition(screenView, set);

        attempts = 0;
        lockMsg.setVisibility(View.GONE);
        alertMsg.setVisibility(View.GONE);
        passcodeEditText.setEnabled(true);
        passcodeEditText.requestFocus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PasscodeFragmentCallback) {
            mListener = (PasscodeFragmentCallback) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement PasscodeFragmentCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onPasscodeInvalid() {
        TransitionSet set = new TransitionSet();
        set.addTransition(new Slide(Gravity.RIGHT));
        TransitionManager.beginDelayedTransition(screenView, set);

        passcodeEditText.setText("");
        attempts += 1;
        alertMsg.setVisibility(View.VISIBLE);

        if (typeParam == PassCodeFragmentType.ENTER_PASSWORD &&
                attempts == MAX_AVAILABLE_ATTEMPTS) {
            MindGeekUserPref.getInstance(getContext()).setAppLocked(true);
            lockApp();

            if (mListener != null) {
                mListener.onAppLocked();
            }
        }
    }

    public PassCodeFragmentType getTypeParam() {
        return typeParam;
    }

    public interface PasscodeFragmentCallback {
        void onPasscodeEntered(String passcode, PassCodeFragmentType type);

        void onAppLocked();
    }

    public enum PassCodeFragmentType {
        NEW_PASSWORD(R.string.passcode_fragment_title_new),
        CONFIRM_PASSWORD(R.string.passcode_fragment_title_confirm),
        ENTER_PASSWORD(R.string.passcode_fragment_title_enter);

        int screenTitleID;

        PassCodeFragmentType(int screenTitleID) {
            this.screenTitleID = screenTitleID;
        }
    }
}
