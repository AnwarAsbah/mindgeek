package com.mindgeek.app.mindgeek;

import android.content.Context;
import android.content.Intent;

/**
 * Created by anwar.asbah on 2019-01-22.
 */

public class SettingsViewPresenter {

    public SettingsView settingsView;
    public String enteredPasscode = "";

    public void attachPasscodeView(SettingsView settingsView) {
        this.settingsView = settingsView;
    }

    public void activatePasscode() {
        if (settingsView != null) {
            settingsView.attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType.NEW_PASSWORD);
        }
    }

    public void deactivatePasscode(Context context) {
        MindGeekUserPref.getInstance(context).setPasscodeEnabled(false);
        MindGeekUserPref.getInstance(context).setPasscode("");
    }

    public void checkPasscodeActivation(Context context) {
        if (settingsView != null) {
            boolean passcodeEnabled = MindGeekUserPref.getInstance(context).isPasscodeEnabled();
            settingsView.setPasscodeEnabled(passcodeEnabled);

            if (passcodeEnabled) {
                settingsView.attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType.ENTER_PASSWORD);
            }

        }
    }

    public void onPasscodeEnter(Context context, String passcode, PasscodeFragment.PassCodeFragmentType type) {
        switch (type) {
            case NEW_PASSWORD:
                if (settingsView != null) {
                    settingsView.detachPasscodeScreenIfAny();
                    settingsView.attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType.CONFIRM_PASSWORD);
                }
                enteredPasscode = passcode;
                break;
            case CONFIRM_PASSWORD:
                if (enteredPasscode != null && passcode != null && enteredPasscode.equals(passcode)) {
                    MindGeekUserPref.getInstance(context).setPasscodeEnabled(true);
                    MindGeekUserPref.getInstance(context).setPasscode(passcode);

                    if (settingsView != null) {
                        settingsView.detachPasscodeScreenIfAny();
                    }
                } else {
                    if (settingsView != null) {
                        settingsView.onPasscodeInvalid();
                    }
                }
                break;
            case ENTER_PASSWORD:
                String userPasscode = MindGeekUserPref.getInstance(context).getPasscode();
                if (passcode != null && userPasscode != null && userPasscode.equals(passcode)) {
                    if (settingsView != null) {
                        settingsView.detachPasscodeScreenIfAny();
                    }
                } else {
                    settingsView.onPasscodeInvalid();
                }
                break;
        }
    }

    public void onAppLocked(Context context) {
        Intent serviceIntent = new Intent(context, LockService.class);
        context.startService(serviceIntent);
    }

    public boolean onBackwarNavigation(Context context, PasscodeFragment.PassCodeFragmentType type) {
        if (type == null || settingsView == null) {
            return false;
        } else {
            switch (type) {
                case NEW_PASSWORD:
                    settingsView.detachPasscodeScreenIfAny();
                    break;
                case CONFIRM_PASSWORD:
                    settingsView.detachPasscodeScreenIfAny();
                    settingsView.attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType.NEW_PASSWORD);
            }

            settingsView.setPasscodeEnabled(MindGeekUserPref.getInstance(context).isPasscodeEnabled());
            return type != PasscodeFragment.PassCodeFragmentType.ENTER_PASSWORD;
        }
    }
}
