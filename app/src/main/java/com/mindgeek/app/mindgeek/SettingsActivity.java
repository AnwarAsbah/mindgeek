package com.mindgeek.app.mindgeek;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class SettingsActivity extends AppCompatActivity implements SettingsView, PasscodeFragment.PasscodeFragmentCallback {

    public static final String LOCK_INTENT_ACTION = "com.mindgeek.brodcast.lock";
    private static final int IMAGE_PICKER_REQUEST_CODE = 0;

    private TextView title;
    private FrameLayout root;
    private Switch passcodeActivationSwitch;
    private View addButton;
    private View noNotesMsg;
    private RecyclerView recyclerView;
    private NotesListAdapter adapter;

    private IntentFilter lockIntent;
    private BroadcastReceiver lockReceiver;

    private PasscodeFragment passcodeFragment;
    private SettingsViewPresenter presenter;

    private CompoundButton.OnCheckedChangeListener passcodeActivationSwitchCheckListener =
            new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    if (checked) {
                        activatePasscode();
                    } else {
                        deactivatePasscode();
                    }
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initView();

        lockIntent = new IntentFilter();
        lockIntent.addAction(LOCK_INTENT_ACTION);

        lockReceiver = new LockBroadcastReceiver();
    }

    private void initView() {
        title = findViewById(R.id.title);
        passcodeActivationSwitch = findViewById(R.id.passcode_activation_switch);
        root = findViewById(R.id.root_view);
        addButton = findViewById(R.id.add_button);
        noNotesMsg = findViewById(R.id.no_notes_msg);
        addButton.setVisibility(View.VISIBLE);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });
        recyclerView = findViewById(R.id.image_list_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new NotesListAdapter();
        recyclerView.setAdapter(adapter);

        title.setText(R.string.settings_activity_title);

        presenter = new SettingsViewPresenter();
        presenter.attachPasscodeView(this);

        root.requestFocus();
        hideKeyboard ();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add a Note:");

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (input.getText().toString().length() > 0) {
                    MindGeekUserPref.getInstance(SettingsActivity.this).addNote(SettingsActivity.this, input.getText().toString());
                }

                if(adapter != null) {
                    adapter.updateData();
                    adapter.notifyDataSetChanged();
                }

                hideKeyboard();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

                hideKeyboard();
            }
        });

        builder.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter.checkPasscodeActivation(this);

        passcodeActivationSwitch.setOnCheckedChangeListener(passcodeActivationSwitchCheckListener);

        if (lockReceiver != null && lockIntent != null) {
            registerReceiver(lockReceiver, lockIntent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        passcodeActivationSwitch.setOnCheckedChangeListener(null);

        if (passcodeFragment != null) {
            Navigator.removeFragment(this, passcodeFragment, root);
        }

        if (lockReceiver != null && lockIntent != null) {
            unregisterReceiver(lockReceiver);
        }
    }

    @Override
    public void attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType type) {
        if (passcodeFragment != null && passcodeFragment.getTypeParam() != type) {
            Navigator.removeFragment(this, passcodeFragment, root);
            passcodeFragment = null;
        }

        if (passcodeFragment == null) {
            passcodeFragment = PasscodeFragment.newInstance(type);
        }

        Navigator.attachFragment(this, passcodeFragment, R.id.root_view, root);
    }

    @Override
    public void setPasscodeEnabled(boolean enabled) {
        passcodeActivationSwitch.setChecked(enabled);
    }

    @Override
    public void onPasscodeInvalid() {
        if (passcodeFragment != null) {
            passcodeFragment.onPasscodeInvalid();
        }
    }

    @Override
    public void detachPasscodeScreenIfAny() {
        if (passcodeFragment != null) {
            Navigator.removeFragment(this, passcodeFragment, root);
            passcodeFragment = null;
        }

        hideKeyboard();
    }

    @Override
    public void onPasscodeEntered(String passcode, PasscodeFragment.PassCodeFragmentType type) {
        if (presenter != null) {
            presenter.onPasscodeEnter(this, passcode, type);
        }
    }

    @Override
    public void onAppLocked() {
        if (presenter != null) {
            presenter.onAppLocked(this);
        }

    }

    @Override
    public void onBackPressed() {

        if (presenter != null) {
            if (presenter.onBackwarNavigation(this, passcodeFragment == null ? null : passcodeFragment.getTypeParam())) {
                return;
            }
        }

        super.onBackPressed();
    }

    private void onAppLockModeChanged() {
        if (passcodeFragment != null) {
            passcodeFragment.updateLockMode();
        }
    }

    private void activatePasscode() {
        if (presenter != null) {
            presenter.activatePasscode();
        }
    }

    private void deactivatePasscode() {
        if (presenter != null) {
            presenter.deactivatePasscode(this);
        }
    }

    private void copyFile(File sourceFile, File destFile) throws IOException {
        if (!sourceFile.exists()) {
            return;
        }

        FileChannel source = null;
        FileChannel destination = null;
        source = new FileInputStream(sourceFile).getChannel();
        destination = new FileOutputStream(destFile).getChannel();
        if (destination != null && source != null) {
            destination.transferFrom(source, 0, source.size());
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }

    }

    public class LockBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            onAppLockModeChanged();
        }
    }

    public class NotesListAdapter extends RecyclerView.Adapter<NotesListAdapter.NotesViewHolder> {

        String[] notes = null;

        public NotesListAdapter () {
            updateData();
        }

        private void updateData() {
            notes = MindGeekUserPref.getInstance(SettingsActivity.this).getNotes();
            noNotesMsg.setVisibility(notes == null || notes.length == 0 ? View.VISIBLE : View.GONE);
        }

        @Override
        public NotesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item, parent, false);
            NotesViewHolder vh = new NotesViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(NotesViewHolder holder, int position) {
            holder.textItem.setText(notes[position]);
            holder.note = notes[position];
        }

        @Override
        public int getItemCount() {
            return notes == null ? 0 : notes.length;
        }

        public class NotesViewHolder extends RecyclerView.ViewHolder {

            TextView textItem;
            View removeButton;

            String note;

            public NotesViewHolder(View itemView) {
                super(itemView);

                textItem = itemView.findViewById(R.id.text_item);
                removeButton = itemView.findViewById(R.id.remove_notes);

                removeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.v("anwar", "onClick");
                        if(note != null) {
                            Log.v("anwar", "onClick2");
                            MindGeekUserPref.getInstance(SettingsActivity.this).removeNote(note);
                            updateData();
                            notifyDataSetChanged();
                        }
                    }
                });

            }
        }
    }
}
