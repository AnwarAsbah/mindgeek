package com.mindgeek.app.mindgeek;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

public class LockService extends Service {

    public static final int LOCK_PERIOD = 60000;

    public LockService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        long remainingPeriod = LOCK_PERIOD - (System.currentTimeMillis() - MindGeekUserPref.getInstance(this).getAppLockTime());
        if (remainingPeriod < 0) {
            stopSelf();
        }

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {

                MindGeekUserPref.getInstance(LockService.this).setAppLocked(false);

                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(SettingsActivity.LOCK_INTENT_ACTION);
                sendBroadcast(broadcastIntent);

                stopSelf();
            }
        }, remainingPeriod);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        long remainingPeriod = LOCK_PERIOD - (System.currentTimeMillis() - MindGeekUserPref.getInstance(this).getAppLockTime());
        if (remainingPeriod < 0) {
            stopSelf();
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
