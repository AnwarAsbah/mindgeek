package com.mindgeek.app.mindgeek;

/**
 * Created by anwar.asbah on 2019-01-22.
 */

public interface SettingsView {
    void attachPasscodeScreen(PasscodeFragment.PassCodeFragmentType type);

    void detachPasscodeScreenIfAny();

    void setPasscodeEnabled(boolean enabled);

    void onPasscodeInvalid();
}
