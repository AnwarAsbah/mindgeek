package com.mindgeek.app.mindgeek;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private static final long SPLASH_DELAY_PERIOD = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        moveToNextWithDelay();
        checkAppLock();
    }

    private void checkAppLock() { //service get killed one app is killed needs to be restarted
        if (MindGeekUserPref.getInstance(this).isAppLocked()) {
            Intent intent = new Intent(this, LockService.class);
            startService(intent);
        }
    }

    private void moveToNextWithDelay() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Navigator.navigateToSettings(SplashActivity.this);
                finish();
            }
        }, SPLASH_DELAY_PERIOD);
    }
}
