package com.mindgeek.app.mindgeek;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.ViewGroup;

import com.transitionseverywhere.Slide;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

/**
 * Created by anwar.asbah on 2019-01-22.
 */

public class Navigator {

    public static void navigateToSettings(@NonNull Context context) {
        context.startActivity(new Intent(context, SettingsActivity.class));
    }

    public static void attachFragment(@NonNull AppCompatActivity context, @NonNull Fragment fragment, int frameID, ViewGroup view) {
        if (view != null) {
            TransitionSet set = new TransitionSet();
            set.addTransition(new Slide(Gravity.RIGHT));
            TransitionManager.beginDelayedTransition(view, set);
        }

        FragmentManager manager = context.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(frameID, fragment);
        transaction.commit();
    }

    public static void removeFragment(@NonNull AppCompatActivity context, @NonNull Fragment fragment, ViewGroup view) {
        if (view != null) {
            TransitionSet set = new TransitionSet();
            set.addTransition(new Slide(Gravity.RIGHT));
            TransitionManager.beginDelayedTransition(view, set);
        }

        FragmentManager manager = context.getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.remove(fragment);
        transaction.commit();
    }
}
